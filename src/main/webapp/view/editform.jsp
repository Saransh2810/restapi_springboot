<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <form action="/form/updateData" method="POST">
          <div class="container">
                <table class="table">
                        <tr>
                            <td><label><b>Id</b></label></td>
                            <td><input type="number" value=${val} name="id" ></td>
                        </tr>
                        <tr>
                            <td><label><b>Name</b></label></td>
                            <td><input type="text" placeholder="Enter your name" name="name" id="name" required></td>
                        </tr>

                        <tr>
                            <td><label><b>Age</b></label></td>
                            <td><input type="number" placeholder="Enter your age" name="age" id="age" required></td>

                        </tr>

                        <tr>
                            <td><label><b>City</b></label></td>
                            <td><input type="text" placeholder="Enter your city" name="city" id="city" required></td>
                        </tr>

                        <tr>
                            <td><label><b>Contact No.</b></label></td>
                            <td><input type="text" placeholder="Enter your contact number" name="contact_number" id="cn" required></td>
                        </tr>
                 </table>


          </div>
            <center><button type="submit" id="editbtn" class="btn btn-success">Edit</button></center>
        </form>

    </body>
</html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>
        <meta charset="utf-8">
                    <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0,
                     minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, target-density ,dpi=device-dpi">



                    <link
                            rel="stylesheet"
                            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
                            integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
                            crossorigin="anonymous"
                    />
                <style>
                    ul li{
                        margin-right:30px;
                        }
                    ul {
                    margin-right:50vw;
                    }
                    .navbar{
                        background-color: white;
                        }
                    .nav-item{
                        font-color:000000;
                        }

                </style>

    </head>
    <body style="background-color: #F1EFF5">
        <nav class="navbar navbar-expand-lg sticky-top">
             <div class="container-fluid">
                  <div class="navbar-header">
                       <a class="navbar-brand" href="#" style="color:#7A9DF8"><img src="/images/bizrise-logo.png" style="height:100;width:120"></a>
                  </div>
                       <ul class="nav navbar-nav justify-content-center" >
                            <li class="nav-item active"><a href="/web/home" ><h2>Home</h2></a></li>
                            <li class="nav-item"><a href="/web/add" style="color:#7A9DF8"><h2>Add Student</h2></a></li>
                       </ul>
             </div>
        </nav><br>
        <form action="insertData" method="POST">
          <div class="container">
            <h1>Student Register</h1>
                    <div class="form-group">
                            <label><b>Name</b></label>
                            <input type="text" class="form-control" placeholder="Enter your name" name="name" id="name" required>
                    </div>

                        <div class="form-group">
                            <label><b>Age</b></label>
                            <input type="number" class="form-control" placeholder="Enter your age" name="age" id="age" required>

                        </div>

                        <div class="form-group">
                            <label><b>City</b></label>
                            <input type="text" class="form-control" placeholder="Enter your city" name="city" id="city" required>
                        </div>

                        <div>
                            <label><b>Contact No.</b></label>
                            <input type="text" class="form-control" placeholder="Enter your contact number" name="contact_number" id="cn" required></td>
                        </div>
                        <div>
                            <label><b>Image</b></label>
                            <input type="text" class="form-control" placeholder="Enter your image" name="image" id="image" required></td>
                        </div><br>


                <center><button type="submit" class="btn btn-success" onclick="alertName()">Save</button></center><br>
          </div>
        </form>


     <script src="https://code.jquery.com/jquery-3.6.0.min.js"
                integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
                crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
                integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
                crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
                integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
                crossorigin="anonymous">
        </script>
        <script type="text/javascript">
            function alertName(){
                alert("Submitted Successfully!!");
            }
        </script>
    </body>
</html>
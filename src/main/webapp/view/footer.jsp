<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>


        <meta charset="utf-8">
            <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0,
             minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, target-density ,dpi=device-dpi">



            <link
                    rel="stylesheet"
                    href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
                    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
                    crossorigin="anonymous"
            />
        <style>
            ul li{
                margin-right:30px;
                }
            ul {
            margin-right:50vw;
            }
            .navbar{
                background-color: white;
                }
            .nav-item{
                font-color:000000;
                }


        </style>
    </head>

    <body style="background-color: #F1EFF5">
        <nav class="navbar navbar-expand-lg sticky-top">
                    <div class="container-fluid">
                      <div class="navbar-header">
                        <a class="navbar-brand" style="color:#7A9DF8" ><img src="/images/bizrise-logo.png" style="height:50;width:50"></a>
                      </div>
                      <ul class="nav navbar-nav justify-content-center" >
                        <li class="nav-item active"><a href="/web/home" style="color:#7A9DF8">Home</a></li>
                        <li class="nav-item"><a href="/web/add" style="color:#7A9DF8">Add Student</a></li>
                      </ul>
                    </div>
                  </nav>
        <div  class="container mt-5">
                <div class="row">

                  <c:forEach var="task" items="${tasks}">
                    <div class="col-lg-3">


                        <div class="card" style="margin-bottom:30px;box-shadow: 0px 2px 6px 0px #1a181e66" >
                            <img class="card-img-top" src=${task.image} height="200" width="286" alt="Card image cap">
                            <div class="card-body">
                                 <div class="d-flex justify-content-end">
                                   <a href="/web/edit/${task.id}" class="card-link"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></a>
                                   <a href="/web/delete/${task.id}" class="card-link"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></a>
                                 </div>
                                <h5 class="card-title">${task.name}</h5>
                                <h6 class="card-text">Id : ${task.id}
                                <h6 class="card-subtitle mb-2 ">Age : ${task.age}</h6>
                                <h6 class="card-text">City : ${task.city}</h6>
                                <h6 class="card-text">Contact No. : ${task.contact_number}
                                </h6>

                            </div>
                        </div>

                    </div>
                  </c:forEach>
                </div>
                </div>

        </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
            integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
            crossorigin="anonymous">
    </script>
    <script src="https://use.fontawesome.com/c970c9d5cb.js"></script>
    </body>
</html>
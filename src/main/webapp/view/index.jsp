<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>
        <script src="../Js/user.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <form >
          <div class="container">
            <h1>Student Register</h1>

                    <table class="table">
                        <tr>
                            <td><label><b>Name</b></label></td>
                            <td><input type="text" placeholder="Enter your name" name="name" id="name" required></td>
                        </tr>

                        <tr>
                            <td><label><b>Age</b></label></td>
                            <td><input type="number" placeholder="Enter your age" name="age" id="age" required></td>

                        </tr>

                        <tr>
                            <td><label><b>City</b></label></td>
                            <td><input type="text" placeholder="Enter your city" name="city" id="city" required></td>
                        </tr>

                        <tr>
                            <td><label><b>Contact No.</b></label></td>
                            <td><input type="text" placeholder="Enter your contact number" name="contact_number" id="cn"required></td>
                        </tr>
                 </table>
                 <center><button type="button" id="registerbtn" class="btn btn-success" onclick="saveStudent()">Save</button></center><br>


          </div>
        </form>

        <center><button type="button"  class="btn btn-warning" onclick="showStudents()">Show</button></center><br>
        <div id="show" class="container"></div>
    </body>
</html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>


    <body>
        <table class="table table-bordered table-striped" id="gettable">
                <tr>
                    <th>ID</th>
                    <th>NAME</th>
                    <th>AGE</th>
                    <th>CITY</th>
                    <th>CONTACT NO.</th>
                </tr>
                <c:forEach var="j" items="${task}">

                    <tr>
                        <td>${j.id}</td>
                        <td>${j.name}</td>
                        <td>${j.age}</td>
                        <td>${j.city}</td>
                        <td>${j.contact_number}</td>
                        <td><a href="/form/formredirect/${j.id}">edit</a></td>
                        <td><a href="/form/delete/${j.id}">delete</a></td>


                    </tr>
                </c:forEach>

            </table>
    </body>
</html>
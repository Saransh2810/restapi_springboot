package nearpe.repository;


import nearpe.model.StudentDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StudentDetailRepository extends CrudRepository<StudentDetails, Long> , JpaRepository<StudentDetails,Long> {

}

package nearpe.service;

import nearpe.exception.ResourceNotFoundException;
import nearpe.model.AreaInfo;
import nearpe.model.StudentDetails;
import nearpe.repository.StudentDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentDetailsService {

    @Autowired
    StudentDetailRepository studentDetailsRepository;

    public StudentDetails adddata(StudentDetails info)  {

        StudentDetails result= this.studentDetailsRepository.save(info);
        return result;

    }
    public List<StudentDetails> findAll(){
        List<StudentDetails> bes = (List<StudentDetails>)studentDetailsRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        return bes;
    }

    public StudentDetails updateRecord(StudentDetails info)
    {
        StudentDetails result= this.studentDetailsRepository.save(info);
        return result;
    }

    public void deleteById(long id) {
        studentDetailsRepository.deleteById(id);
    }

    public StudentDetails saveStudent(String name, int age, String city, String contact_number){
        StudentDetails inf=new StudentDetails();

        inf.setName(name);
        inf.setAge(age);
        inf.setCity(city);
        inf.setContact_number(contact_number);
        studentDetailsRepository.save(inf);
        return inf;
    }

    public Optional<StudentDetails> findById(long id) {
        Optional<StudentDetails> inf=studentDetailsRepository.findById(id);
        return inf;
    }
}

package nearpe.controller;

import nearpe.model.StudentDetails;
import nearpe.service.StudentDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
@RequestMapping("/web")
public class AppController
{
    @Autowired
    StudentDetailsService studentDetailsService;




    @RequestMapping("/add")
    public String Add() {

        return "add";
    }

    @PostMapping("/insertData")
    public String addData(@RequestParam(required = false) String name,
                          @RequestParam(required = false) int age,@RequestParam(required = false) String city,
                          @RequestParam(required = false) String contact_number,@RequestParam(required = false) String image)
    {
        StudentDetails sd=new StudentDetails();
        sd.setAge(age);
        sd.setName(name);
        sd.setCity(city);
        sd.setContact_number(contact_number);
        sd.setImage(image);
        StudentDetails b=this.studentDetailsService.adddata(sd);
        System.out.print(b);
        return "redirect:/web/home";

    }
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView taskList() {

        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("tasks",studentDetailsService.findAll());
        modelAndView.setViewName("footer");
        return modelAndView;
    }

    @RequestMapping(value="/edit/{id}")
    public ModelAndView redirect(@PathVariable long id)
    {
        ModelAndView modelAndView=new ModelAndView();
        Optional<StudentDetails> inf=studentDetailsService.findById(id);
        modelAndView.addObject("val",inf.get());
        modelAndView.setViewName("update");
        return modelAndView;
    }
    @PostMapping("/update")
    public String updateData(@RequestParam(required = false) long id,@RequestParam(required = false) String name,
                             @RequestParam(required = false) int age,@RequestParam(required = false) String city,
                             @RequestParam(required = false) String contact_number,@RequestParam(required = false) String image)
    {
        StudentDetails sd=new StudentDetails();
        if(id!=0)
            sd.setId(id);
        if(name!=null)
            sd.setName(name);
        if(age!=0)
            sd.setAge(age);
        if(city!=null)
            sd.setCity(city);
        if(contact_number!=null)
            sd.setContact_number(contact_number);
        if(image!=null)
            sd.setImage(image);
        System.out.println("working");
        studentDetailsService.updateRecord(sd);
        return "redirect:/web/home";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable long id) {
        studentDetailsService.deleteById(id);
        return "redirect:/web/home";
    }

}
//https://play.google.com/store/apps/details?id=my.bikry.app
//        https://suraj_store.bikry.com/

//http://storemanager.bizrise.in/

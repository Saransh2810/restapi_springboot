package nearpe.controller;

/*import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FormController {

    @RequestMapping("/fork")
    public String form(Model model)
    {
        model.addAttribute("title","Form Page");
        return "form";
    }
}*/

import nearpe.model.StudentDetails;
import nearpe.service.StudentDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;
@Controller
@RequestMapping("/form")
public class FormController {


    @Autowired
    StudentDetailsService studentDetailsService;
    @RequestMapping("/user")
    public String Form() {

        return "form";
    }

    /*@RequestMapping("/addData")
    public String addData(StudentDetails details) {
        StudentDetails b=this.studentDetailsService.adddata(details);
        System.out.print(b);
        return "form";
    }*/

    @RequestMapping("/addData")
    public String addData(@RequestParam(required = false) String name,
                          @RequestParam(required = false) int age,@RequestParam(required = false) String city,
                          @RequestParam(required = false) String contact_number)
    {
        //Map<String, Object> model = this.getDataModel("products");
       // ModelAndView model = new ModelAndView();
        StudentDetails sd=new StudentDetails();
        //sd.setId(id);
        sd.setAge(age);
        sd.setName(name);
        sd.setCity(city);
        sd.setContact_number(contact_number);
        StudentDetails b=this.studentDetailsService.adddata(sd);
        System.out.print(b);
        return "table";

    }
    @RequestMapping(value = "/tas", method = RequestMethod.GET)
    public ModelAndView taskList() {

        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("task",studentDetailsService.findAll());
        modelAndView.setViewName("getall");
        return modelAndView;
    }

    @PostMapping("/updateData")
    public String updateData(@RequestParam(required = false) int id,@RequestParam(required = false) String name,
                          @RequestParam(required = false) int age,@RequestParam(required = false) String city,
                          @RequestParam(required = false) String contact_number)
    {
        StudentDetails sd=new StudentDetails();
        if(id!=0)
            sd.setId(id);
        if(name!=null)
            sd.setName(name);
        if(age!=0)
            sd.setAge(age);
        if(city!=null)
            sd.setCity(city);
        if(contact_number!=null)
            sd.setContact_number(contact_number);
        System.out.println("working");
        studentDetailsService.updateRecord(sd);
        return "redirect:/form/tas";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable long id) {
        studentDetailsService.deleteById(id);
        return "redirect:/form/tas";
    }

    @RequestMapping(value="/formredirect/{id}")
    public ModelAndView redirect(@PathVariable long id)
    {
        ModelAndView modelAndView=new ModelAndView();

        modelAndView.addObject("val",id);
        modelAndView.setViewName("editform");
        return modelAndView;
    }
}

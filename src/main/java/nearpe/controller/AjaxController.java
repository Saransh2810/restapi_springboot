package nearpe.controller;


import nearpe.model.StudentDetails;
import nearpe.service.StudentDetailsService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/ajax")
public class AjaxController {
    @Autowired
    StudentDetailsService studentService;

    @GetMapping("/get")
    public String indexform()
    {

        return "index";
    }
    @PostMapping("/save")
    public @ResponseBody String saveUser(@RequestParam("name") String name,@RequestParam("age") int age,
                                         @RequestParam("city") String city,
                                         @RequestParam("contact_number") String contact_number)throws JSONException
    {
        StudentDetails inf=studentService.saveStudent(name,age,city,contact_number);
        JSONObject obj=new JSONObject();


        obj.put("name",inf.getName());
        obj.put("age",inf.getAge());
        obj.put("city",inf.getCity());
        obj.put("contact_number",inf.getContact_number());

        return  obj.toString();

    }

    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public @ResponseBody List<StudentDetails> show() {

       List<StudentDetails> list=studentService.findAll();
       return list;
    }

    @RequestMapping(value = "/deleteAjax/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable long id) {

        studentService.deleteById(id);
        return "redirect:/ajax/get";
    }

    @RequestMapping(value="/editAjax/{id}")
    public ModelAndView redirect(@PathVariable long id)
    {
        ModelAndView modelAndView=new ModelAndView();
        Optional<StudentDetails> inf=studentService.findById(id);
        modelAndView.addObject("val",inf.get());
        modelAndView.setViewName("updateform");
        return modelAndView;
    }

    @PostMapping("/updateAjax")
    public @ResponseBody StudentDetails updateData(@RequestParam(required = false) long id,@RequestParam(required = false) String name,
                             @RequestParam(required = false) int age,@RequestParam(required = false) String city,
                             @RequestParam(required = false) String contact_number)
    {
        StudentDetails sd=new StudentDetails();
        if(id!=0)
            sd.setId(id);
        if(name!=null)
            sd.setName(name);
        if(age!=0)
            sd.setAge(age);
        if(city!=null)
            sd.setCity(city);
        if(contact_number!=null)
            sd.setContact_number(contact_number);
        System.out.println("working");
        StudentDetails res=studentService.updateRecord(sd);
        return res;
    }



}